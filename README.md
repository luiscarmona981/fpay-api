# fpay-rest

Api cuyo fin es escontrar números primos

## Instalación

Instalar dependencias

```bash
 npm i
```

## Ejecución local

Para ejecutar localmente el servicio se utiliza el siguiente comando :

```bash
npm run local
```

## Ejecución de pruebas unitarias y coverage

```bash
npm run test
```

### Variables de entorno

Las siguientes variables necesitan ser definidas:

    PORT=3000
    URL_GLOBAL=/bff/v1/fpay-bff

### Postman

metodo

```
POST
```

ruta

```
http://localhost:3001/bff/v1/fpay-bff/primeNumber
```

body

```
{
"number": 100
}
```